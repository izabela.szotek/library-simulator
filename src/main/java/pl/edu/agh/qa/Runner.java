package pl.edu.agh.qa;

import pl.edu.agh.qa.items.Book;
import pl.edu.agh.qa.items.Magazine;
import pl.edu.agh.qa.users.Lecturer;
import pl.edu.agh.qa.users.Student;

public class Runner {
    public static void main(String[] args) {

        Library library = new Library();

        Student s1 = new Student("Andrzej", "Sikora");
        Lecturer l1 = new Lecturer("Roman", "Kuś");

        library.addUserToLibrary(l1, s1);

        System.out.println("List of users:");
        library.printListOfUsers();
        System.out.println();

        Book b1 = new Book("Stephen King", "It");
        Book b2 = new Book("Lucy Maud Montgomery", "Anne of Green Gables");
        Book b3 = new Book("Stephen King", "It");

        Magazine m1 = new Magazine("01/2021", "National Geographic");
        Magazine m2 = new Magazine("1/2020", "Lego Explorer");

        library.addItemToLibrary(b1, b2, b2, b2, m1, m2, b3);


        library.importItemsFromFile("C:/Docs/NewItems.csv");

        System.out.println("List of magazines:");
        library.printListOfMagazines();

        System.out.println();

        System.out.println("List of books:");
        library.printListOfBooks();

        System.out.println(library.rentItemToUser(b1, l1));
        System.out.println(library.rentItemToUser(m2, s1));
        System.out.println(library.rentItemToUser(m2, l1));
        System.out.println(library.rentItemToUser(b2, s1));
        System.out.println(library.rentItemToUser(m1, s1));
        System.out.println(library.rentItemToUser(b2, s1));
        System.out.println(library.rentItemToUser(b2, s1));
        System.out.println(library.rentItemToUser(b2, s1));
        System.out.println(library.rentItemToUser(m2, l1));
        System.out.println(library.rentItemToUser(new Book("H. Sienkiewicz", "Ogniem  i mieczem"), l1));
        System.out.println(library.rentItemToUser(new Book("H. Sienkiewicz", "Ogniem i mieczem"), l1));


        System.out.println("List of magazines:");
        library.printListOfMagazines();

        System.out.println();

        System.out.println("List of books:");
        library.printListOfBooks();

        library.exportUsersWithItemsToFile("C:\\Workspace\\library-simulator\\src\\main\\java\\pl\\edu\\agh\\qa\\docs\\usersWithBooks.csv");

    }

}
