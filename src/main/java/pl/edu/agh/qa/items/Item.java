package pl.edu.agh.qa.items;

public abstract class Item {
    abstract public String toString();

    abstract public String getTitle();

    abstract public String getNumberOrAuthor();
}