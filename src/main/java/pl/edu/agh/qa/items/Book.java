package pl.edu.agh.qa.items;

import static pl.edu.agh.qa.Library.NAMES_SEPARATOR;

public class Book extends Item {
    private String title;
    private String author;

    public Book(String author, String title) {
        this.author = author;
        this.title = title;
    }

    @Override
    public String toString() {
        return title + NAMES_SEPARATOR + author;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getNumberOrAuthor() {
        return author;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return title.equals(book.title) && (author.equals(book.author));
    }

    @Override
    public int hashCode() {
        return title.length();
    }
}