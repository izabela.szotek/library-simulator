package pl.edu.agh.qa.items;

import static pl.edu.agh.qa.Library.NAMES_SEPARATOR;

public class Magazine extends Item {
    private String title;
    private String number;

    public Magazine(String number, String title) {
        this.number = number;
        this.title = title;
    }

    @Override
    public String toString() {
        return title + NAMES_SEPARATOR + number;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getNumberOrAuthor() {
        return number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Magazine magazine = (Magazine) o;
        return title.equals(magazine.title) && (number.equals(magazine.number));
    }

    @Override
    public int hashCode() {
        return title.length();
    }
}