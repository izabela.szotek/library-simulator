package pl.edu.agh.qa.users;

public abstract class User {
    protected String firstName;
    protected String lastName;
    protected String cardId;
    public static int nextId = 1;

    public User(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.cardId = distributeId();
    }

    abstract public String toString();

    abstract public int getRentLimit();

    public String getCardId() {
        return cardId;
    }

    public static String distributeId() {
        String output = Integer.toString(nextId);
        while (output.length() < 4) output = "0" + output;
        nextId++;
        return "ID" + output;
    }
}