package pl.edu.agh.qa.users;

import java.util.Objects;

import static pl.edu.agh.qa.Library.NAMES_SEPARATOR;

public class Student extends User {
    private int rentLimit = 4;

    public Student(String firstName, String lastName) {
        super(firstName, lastName);
    }

    @Override
    public String toString() {
        return firstName + NAMES_SEPARATOR + lastName + NAMES_SEPARATOR + cardId;
    }

    @Override
    public int getRentLimit() {
        return rentLimit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return cardId.equals(student.cardId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lastName);
    }
}