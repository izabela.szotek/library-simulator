package pl.edu.agh.qa.users;

import java.util.Objects;

import static pl.edu.agh.qa.Library.NAMES_SEPARATOR;

public class Lecturer extends User {
    private int rentLimit = 10;

    public Lecturer(String firstName, String lastName) {
        super(firstName, lastName);
    }

    @Override
    public String toString() {
        return firstName + NAMES_SEPARATOR + lastName + NAMES_SEPARATOR + cardId;
    }

    @Override
    public int getRentLimit() {
        return rentLimit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Lecturer lecturer = (Lecturer) o;
        return cardId.equals(lecturer.cardId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lastName);
    }
}