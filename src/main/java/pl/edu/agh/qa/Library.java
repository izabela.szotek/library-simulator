package pl.edu.agh.qa;

import pl.edu.agh.qa.items.Book;
import pl.edu.agh.qa.items.Item;
import pl.edu.agh.qa.items.Magazine;
import pl.edu.agh.qa.users.Lecturer;
import pl.edu.agh.qa.users.User;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Library {
    public static final String NAMES_SEPARATOR = ";";
    public static final String LECTURER_TYPE_MARKER = "L";
    public static final String STUDENT_TYPE_MARKER = "S";
    public static final String BOOK_TYPE_MARKER = "B";

    public static final int AUTHOR_POSITION = 0;
    public static final int AUTHOR_OR_NUMBER_POSITION = 1;
    public static final int NUMBER_OF_ITEMS_POSITION = 2;
    public static final int TYPE_POSITION = 3;


    private Map<Item, ItemAvailability> itemsAvailability = new HashMap<>();
    private Map<User, List<Item>> usersWithItems = new HashMap<>();

    class ItemAvailability {
        private int all;
        private int available;

        public ItemAvailability(int all, int available) {
            this.all = all;
            this.available = available;
        }

        public void increaseBy(int by) {
            all += by;
            available += by;
        }

        public String toString() {
            return all + NAMES_SEPARATOR + available;
        }

        public int getAvailable() {
            return available;
        }

        public void descreaseAvailable(int by) {
            this.available -= by;
        }
    }

    public Library() {
    }

    public void addUserToLibrary(User... users) {
        for (User user : users) {
            if (!(usersWithItems.keySet().contains(user))) {
                usersWithItems.put(user, new ArrayList() {
                });
            }
        }
    }

    public void addItemToLibrary(Item... items) {
        for (Item item : items) {
            if (itemsAvailability.containsKey(item)) {
                ItemAvailability current = itemsAvailability.get(item);
                current.increaseBy(1);
                itemsAvailability.put(item, current);
            } else {
                itemsAvailability.put(item, new ItemAvailability(1, 1));
            }
        }
    }

    public boolean rentItemToUser(Item item, User user) {
        if (isItemAvailable(item) && isUserEligibleForRent(user)) {
            ItemAvailability availability = itemsAvailability.get(item);
            availability.descreaseAvailable(1);
            itemsAvailability.put(item, availability);
            List<Item> rentedItems = usersWithItems.get(user);
            rentedItems.add(item);
            usersWithItems.put(user, rentedItems);
            return true;
        }
        return false;
    }

    private boolean isItemAvailable(Item item) {
        return itemsAvailability.containsKey(item) && itemsAvailability.get(item).getAvailable() > 0;
    }

    private boolean isUserEligibleForRent(User user) {
        return usersWithItems.containsKey(user) && usersWithItems.get(user).size() < user.getRentLimit();
    }

    public void printListOfMagazines() {
        for (Item item : itemsAvailability.keySet()) {
            if (item instanceof Magazine) {
                System.out.println(item.toString() + NAMES_SEPARATOR + itemsAvailability.get(item).toString());
            }
        }
    }

    public void printListOfBooks() {
        for (Item item : itemsAvailability.keySet()) {
            if (item instanceof Book) {
                System.out.println(item.toString() + NAMES_SEPARATOR + itemsAvailability.get(item).toString());
            }
        }
    }

    public void printListOfUsers() {
        String type;
        for (User user : usersWithItems.keySet()) {
            if (user instanceof Lecturer) {
                type = LECTURER_TYPE_MARKER;
            } else {
                type = STUDENT_TYPE_MARKER;
            }
            System.out.println(user.toString() + NAMES_SEPARATOR + type);
        }
    }

    public void importItemsFromFile(String csvFile) {

        File fileWithItems = new File(csvFile);
        try (FileReader fileReader = new FileReader(fileWithItems);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] csvRow = line.split(NAMES_SEPARATOR);
                Item itemToAdd = parseLineToItem(csvRow);
                int numberOfItems = extractNumberOfItems(csvRow);
                addItemsToLibraryByNumber(itemToAdd, numberOfItems);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int extractNumberOfItems(String[] csvRow) {
        return Integer.parseInt(csvRow[NUMBER_OF_ITEMS_POSITION]);
    }

    private Item parseLineToItem(String[] csvRow) {
        String title = csvRow[AUTHOR_POSITION];
        String authorOrNumber = csvRow[AUTHOR_OR_NUMBER_POSITION];
        String type = csvRow[TYPE_POSITION];
        if (BOOK_TYPE_MARKER.equals(type)) {
            return new Book(authorOrNumber, title);
        } else
            return new Magazine(authorOrNumber, title);
    }

    private void addItemsToLibraryByNumber(Item item, int by) {
        if (itemsAvailability.containsKey(item)) {
            ItemAvailability availability = itemsAvailability.get(item);
            availability.increaseBy(by);
            itemsAvailability.put(item, availability);
        } else {
            itemsAvailability.put(item, new ItemAvailability(by, by));
        }
    }

    public void exportUsersWithItemsToFile(String csvFile) {
        File fileWithID = new File(csvFile);
        try (
                FileWriter fileWriter = new FileWriter(fileWithID, false);
                BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            for (User user : usersWithItems.keySet()) {
                List<Item> userItems = usersWithItems.get(user);
                if (userItems.size() > 0) {
                    bufferedWriter.write(user.getCardId() + usersItemsToString(userItems));
                    bufferedWriter.newLine();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String usersItemsToString(List<Item> userItems) {
        userItems.toArray();
        String record = "";
        for (Item item : userItems) {
            record += item.getTitle() + "-" + item.getNumberOrAuthor() + NAMES_SEPARATOR + " ";
        }
        return "[" + record.substring(0, record.length() - 2) + "]";
    }
}